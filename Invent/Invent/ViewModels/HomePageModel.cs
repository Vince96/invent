﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Invent
{
    class HomePageModel : FreshMvvm.FreshBasePageModel
    {

        // ICommand implementations
        public ICommand StartClickedCommand { protected set; get; }
        public ICommand ListClickedCommand { protected set; get; }

        public HomePageModel()
        {
            StartClickedCommand = new Command(async () =>
            {
                await CoreMethods.PushPageModel<AddPageModel>();
            });
            ListClickedCommand = new Command(async() =>
            {
                await CoreMethods.PushPageModel<InventoryListPageModel>();
            });
        }
    }
}
