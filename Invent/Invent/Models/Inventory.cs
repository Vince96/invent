﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Invent
{
    class Inventory
    {
        public Inventory()
        {
            Created = DateTime.Now;
        }
        public List<InventoryItem> Items { get; set; }
        public DateTime Created{ get; set; }
        public string Title { get; set; }
    }
}
